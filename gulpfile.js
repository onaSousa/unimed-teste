
var gulp         = require('gulp'),
    browserSync  = require('browser-sync'),
    jshint       = require('gulp-jshint'),
    uglify       = require('gulp-uglify'),
    concat       = require('gulp-concat'),
    plumber      = require('gulp-plumber'),
    changed      = require('gulp-changed'),
    imagemin     = require('gulp-imagemin'),
    size         = require('gulp-size'),
    sass         = require('gulp-sass'),
    postcss      = require('gulp-postcss'), 
    autoprefixer = require('autoprefixer'),
    gcmq         = require('gulp-group-css-media-queries'),
    cssvariables = require('postcss-css-variables'), 
    calc         = require('postcss-calc'),
    cssnano      = require('cssnano');


gulp.task('styles', function () {
  gulp.src('src/scss/**/*.scss')
    .pipe(sass().on('error', sass.logError))
    .pipe(gcmq())
    .pipe(postcss([
      autoprefixer(), 
      cssvariables({ preserve: true }), 
      calc(), 
      cssnano()
    ]))
    .pipe(gulp.dest('public/css'))
    .pipe(browserSync.reload({ stream: true }));
});

gulp.task('scripts', function() {
  return gulp.src(['src/js/**/*.js'])
    //.pipe(jshint('.jshintrc'))
    //.pipe(jshint.reporter('default'))
    .pipe(plumber())
    .pipe(concat('main.js'))
    .pipe(uglify())
    .pipe(gulp.dest('public/js'))
    .pipe(browserSync.reload({stream:true}));
});

// Optimizes the images that exists
gulp.task('images', function () {
  return gulp.src('src/images/**')
    .pipe(changed('public/images'))
    .pipe(imagemin({
      // Lossless conversion to progressive JPGs
      progressive: true,
      // Interlace GIFs for progressive rendering
      interlaced: true
    }))
    .pipe(gulp.dest('public/images'))
    .pipe(size({title: 'images'}));
});

gulp.task('html', function() {
  gulp.src('src/**/*.html')
    .pipe(gulp.dest('public/'))
});

gulp.task('browser-sync', ['styles', 'scripts'], function() {
  browserSync({
    server: {
      baseDir: "./public/",
      injectChanges: true // this is new
    }
  });
});

gulp.task('watch', function() {
  // Watch .html files
  gulp.watch('src/**/*.html', ['html', browserSync.reload]);
  gulp.watch("public/*.html").on('change', browserSync.reload);
  // Watch scss files
  gulp.watch('src/scss/**/*.scss', ['styles', browserSync.reload]);
  // Watch .js files
  gulp.watch('src/js/*.js', ['scripts', browserSync.reload]);
  // Watch image files
  gulp.watch('src/images/**/*', ['images', browserSync.reload]);
});

gulp.task('default', function() {
    gulp.start('styles', 'scripts', 'images', 'html', 'browser-sync', 'watch');
});
