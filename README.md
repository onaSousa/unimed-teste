# Unimed - Web design | Jonas Sousa

* Data: 01/07/2020
* Standards: HTML(X), CSS(X), JS
* Components: Gulp, SASS, JavaScript
* Software: VS Code, Photoshop, Vivaldi

## Teste

Criar uma Landing page responsiva utilizando HTML e CSS, com base no Modelo em PSD que se encontra na pasta. Será avaliado a semântica do código e a fidelidade com o modelo estabelecido.

Opcional: Utilizar framework front-end de sua preferência.


## Jonas Sousa

Design de interfaces e Front-end

* [Portfólio](https://www.behance.net/onasousa)
* E-mail - jonascomdominio@gmail.com

---

### Instalar

````
npm install
````

Aqui baixa todos os pacotes do Node. Claro, considerando que tenha o Node já instalado.

### Usar

````
gulp
````
Rodando o comando gulp você vai ter site sendo acompanhado e compilando via BrowserSync.

#### Você pode tambem ir para a pasta /public e abrir o index.html